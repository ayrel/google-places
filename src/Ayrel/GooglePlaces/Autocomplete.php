<?php

namespace Ayrel\GooglePlaces;

class Autocomplete extends GoogleApi
{
	protected $key;
	protected $params = array('types' => '(cities)');
	protected $baseUrl = "https://maps.googleapis.com/maps/api/place/autocomplete";

	public function search($input)
	{
		$this->params['input'] = $input;

		return $this->request();
	}

	public function getCity($input)
	{
		$results = $this->search($input);

		$fisrt = $results->predictions[0];

		return City::createById($fisrt->place_id, $this->key);
	}

}
