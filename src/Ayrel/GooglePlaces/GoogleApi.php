<?php

namespace Ayrel\GooglePlaces;

class GoogleApi
{
	protected $key;

	protected $baseUrl;

	protected $params;

	protected $output = 'json';

	protected $browser;

	public function __construct($key)
	{
		$this->setKey($key);
		$this->setParameter('key', $this->getKey());
		$this->browser = new \Buzz\Browser( new \Buzz\Client\Curl() );
	}

	public function getUrl()
	{
		return $this->baseUrl ."/". $this->output. "?". http_build_query($this->params);
	}

	public function request()
	{
		$url = $this->getUrl();
		$response = $this->browser->get($url);
		$result = json_decode($response->getContent());
		if($result->status != "OK") 
			throw new \Exception(
				sprintf( 
					"request error: %s on url : %s",
					$result->status,
					$url
				)
			);
		return $result;
	}

	public function setParameter($key, $value)
	{
		$this->params[$key] = $value;
	}

	public function setKey($key)
	{
	    $this->key = $key;
	    return $this;
	}
	
	public function getKey()
	{
	    return $this->key;
	}
}
