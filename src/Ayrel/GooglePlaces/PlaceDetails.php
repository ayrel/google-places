<?php

namespace Ayrel\GooglePlaces;

class PlaceDetails extends GoogleApi
{
	protected $baseUrl = "https://maps.googleapis.com/maps/api/place/details";

	public function getPlace($id)
	{
		$this->params['placeid'] = $id;

		return $this->request();
	}
}
