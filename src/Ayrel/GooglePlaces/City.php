<?php

namespace Ayrel\GooglePlaces;

class City
{
	protected $name;

	protected $formattedAddress;

	protected $latitude;

	protected $longitude;

	protected $country;

	static function createById($placeId, $key)
	{
		$pd = new PlaceDetails($key);
		$response = $pd->getPlace($placeId);

		$city = new City();
		$city->setName($response->result->name);
		$city->setLatitude($response->result->geometry->location->lat);
		$city->setLongitude($response->result->geometry->location->lng);
		$city->setFormattedAddress($response->result->formatted_address);

		$country = self::findCountry($response->result->address_components);
		$city->setCountry($country);

		return $city;
	}

	static public function findCountry($address_component)
	{
		foreach($address_component as $add)
		{
			if(in_array("country", $add->types)) return $add->short_name; 
		}
	}

	public function setName($name)
	{
	    $this->name = $name;
	    return $this;
	}
	
	public function getName()
	{
	    return $this->name;
	}

	public function setFormattedAddress($formattedAddress)
	{
	    $this->formattedAddress = $formattedAddress;
	    return $this;
	}
	
	public function getFormattedAddress()
	{
	    return $this->formattedAddress;
	}

	public function setLatitude($latitude)
	{
	    $this->latitude = $latitude;
	    return $this;
	}
	
	public function getLatitude()
	{
	    return $this->latitude;
	}

	public function setLongitude($longitude)
	{
	    $this->longitude = $longitude;
	    return $this;
	}
	
	public function getLongitude()
	{
	    return $this->longitude;
	}

	public function setCountry($country)
	{
	    $this->country = $country;
	    return $this;
	}
	
	public function getCountry()
	{
	    return $this->country;
	}
}
