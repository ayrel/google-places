<?php

namespace Ayrel\Tests;
use Ayrel\GooglePlaces\Autocomplete;

class AutocompleteTest extends \PHPUnit_Framework_TestCase
{
	protected $key = "AIzaSyBl97zZldpc1Z6yfG-Fre_kGhHw3nL5zjc";

	public function testSearch()
	{
		$ga = new Autocomplete("AIzaSyBl97zZldpc1Z6yfG-Fre_kGhHw3nL5zjc");
		$response = $ga->search('Paris, FR');

		$this->assertEquals	($response->predictions[0]->description, "Paris, France");
	}

	/**
	 * @dataProvider provider
	 */
	public function testGetCity($request, $lat, $long)
	{
		$ga = new Autocomplete("AIzaSyBl97zZldpc1Z6yfG-Fre_kGhHw3nL5zjc");
		$city = $ga->getCity($request);

		$this->assertEquals($city->getLatitude(), $lat);
		$this->assertEquals($city->getLongitude(), $long);
	}

	public function provider()
	{
		return array(
			array('Lyon, FR', 45.764043, 4.835659),
			array('St Chamond, FR', 45.476088, 4.5127119),
			array('Amsterdam, NL', 52.3702157, 4.8951679)
		);
	}
}
