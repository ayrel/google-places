<?php

namespace Ayrel\Tests;

use Ayrel\GooglePlaces\PlaceDetails;

class PlaceDetailTest extends \PHPUnit_Framework_TestCase
{
	public function testGetPlace()
	{
		$pd = new PlaceDetails("AIzaSyBl97zZldpc1Z6yfG-Fre_kGhHw3nL5zjc");
		$result = $pd->getPlace("ChIJD7fiBh9u5kcRYJSMaMOCCwQ");

		$this->assertEquals($result->result->name, "Paris");
	}
}
