<?php

namespace Ayrel\Tests;

use Ayrel\GooglePlaces\City;

class CityTest extends \PHPUnit_Framework_TestCase
{
	public function testCreateById()
	{
		$city = City::createById("ChIJD7fiBh9u5kcRYJSMaMOCCwQ", "AIzaSyBl97zZldpc1Z6yfG-Fre_kGhHw3nL5zjc");

		$this->assertEquals($city->getLatitude(), 48.856614);
		$this->assertEquals($city->getLongitude(), 2.3522219);
	}
}